#!/bin/bash
XML1=a01-roomlist.xml 
XSD1=a01-roomlist.xsd
XML2=a02-roomlist.xml 
XSD2=a02-roomlist.xsd 
XML3=a03-roomschedule.xml
XSD3=a03-roomschedule.xsd
XML4=a04-roomschedule.xml
XSD4=a04-roomschedule.xsd
OPTCONFIG="--config"

usage() {
echo "Usage: $0 --config=<nb>"
	echo " <nb>=1 : validate $XML1 against $XSD1" 
	echo " <nb>=2 : validate $XML2 against $XSD2" 
	echo " <nb>=3 : validate $XML3 against $XSD3" 
	echo " <nb>=4 : validate $XML4 against $XSD4" 
}
if [ "$1" = "" ]; then usage; exit 1; fi

# get option name as the prefix in front of character =
PRFX="${1%%=*}"
#echo PRFX=$PRFX

if [ "$PRFX" != "$OPTCONFIG" ]; then usage; exit 1; fi

# get value of option
SUFFX="${1#*=}"
#echo SUFFX=$SUFFX

case "$SUFFX" in
	1) XMLFILE=$XML1 XSDSCHEMA=$XSD1;;
	2) XMLFILE=$XML2 XSDSCHEMA=$XSD2;;
	3) XMLFILE=$XML3 XSDSCHEMA=$XSD3;;
	4) XMLFILE=$XML4 XSDSCHEMA=$XSD4;;
	*) 	echo "ERROR: Found unknown value for $OPTCONFIG='$SUFFX'"; usage; exit 1;;
esac

CMD="xmllint --noout --schema $XSDSCHEMA $XMLFILE"
echo "--- Going to execute: $CMD"
$CMD
