﻿<?xml version="1.0" ?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="text" />

<xsl:template match="/">
<xsl:apply-templates select="BILDSCHRIM" />
</xsl:template>


<xsl:template match="BILDSCHRIM">
<xsl:apply-templates select="ANZEIGE" />
</xsl:template>

<xsl:template match="ANZEIGE">
<xsl:value-of select="." /> <xsl:text>&#13;</xsl:text>
</xsl:template>

</xsl:stylesheet>

