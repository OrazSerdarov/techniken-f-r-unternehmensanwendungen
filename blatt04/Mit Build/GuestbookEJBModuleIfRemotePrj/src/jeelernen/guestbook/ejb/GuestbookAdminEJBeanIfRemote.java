/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jeelernen.guestbook.ejb;

import javax.ejb.Remote;
import jeelernen.guestbook.GuestbookEntry;

/**
 *
 * @author vmuser
 */

@Remote
public interface GuestbookAdminEJBeanIfRemote {

    public GuestbookEntry createEntry(String author, String msg);

    public GuestbookEntry modifyEntry(int id, String author, String msg);

    public GuestbookEntry deleteEntry(int id);

    public int getCountCreateEntry();

    public int getCountModifyEntry();

    public int getCountDeleteEntry();

    public int getCountTotal();
}
