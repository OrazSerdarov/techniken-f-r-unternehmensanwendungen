/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jeelernen.guestbook.ejb;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.ejb.Remote;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import jeelernen.guestbook.GuestbookEntry;

/**
 *
 * @author vmuser
 */
@Stateless
public class GuestbookAdminEJBean implements GuestbookAdminEJBeanIfRemote {

    @EJB
    private GuestbookEngineEJBeanIfLocal adminEJBean;
    private int countDelete ;
    private int countModyfy;
    private int countCreate;

    public GuestbookAdminEJBean() {
        countDelete=0;
        countModyfy=0;
        countCreate=0;

    }


    @Override
    public GuestbookEntry createEntry(String author, String msg) {
        countCreate++;
        return adminEJBean.createEntry(author, msg);
    }

    @Override
    public GuestbookEntry modifyEntry(int id, String author, String msg) {
        countModyfy++;
        return adminEJBean.modifyEntry(id, author, msg);
    }

    @Override
   // @Interceptors(MyInterceptor.class)
    public GuestbookEntry deleteEntry(int id) {
        countDelete++;
        return adminEJBean.deleteEntry(id);
    }

    @Override
    public int getCountCreateEntry() {
        return this.countCreate;
    }

    @Override
    public int getCountModifyEntry() {
       return this.countModyfy;
    }

    @Override
    public int getCountDeleteEntry() {
        return this.countDelete;
    }

    @Override
    public int getCountTotal() {
     return this.countCreate + this.countDelete + this.countModyfy;
    }


}
