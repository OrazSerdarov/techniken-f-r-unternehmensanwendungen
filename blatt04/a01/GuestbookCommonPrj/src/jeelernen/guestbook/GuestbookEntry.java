/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jeelernen.guestbook;

/**
 *
 * @author vmuser
 */
import java.util.Date;
import java.io.Serializable;


public class GuestbookEntry implements Serializable {

   

    private String author;
    private String msg;
    private int id;
    private Date date;

    public GuestbookEntry(String author, String msg, int id , Date date) {
        this.author = author;
        this.msg = msg;
        this.id = id;
        this.date = date;

    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    
    public boolean  equals(Object o){
        boolean ret= false;
        if(o!=null){
            if(o instanceof GuestbookEntry){
                GuestbookEntry ge = (GuestbookEntry)o;
                ret= (this.id == ge.getId());
            }
        }
        
        return ret;
    }

}
