/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jeelernen.guestbook.ejb;

import javax.ejb.Local;
import jeelernen.guestbook.GuestbookEntry;

/**
 *
 * @author vmuser
 */
@Local
public interface GuestbookEngineEJBeanIfLocal {

    public GuestbookEntry createEntry(String author, String msg);

    public GuestbookEntry modifyEntry(int id, String author, String msg);

    public GuestbookEntry deleteEntry(int id);
}
