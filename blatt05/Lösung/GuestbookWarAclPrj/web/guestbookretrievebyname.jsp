<%-- 
    Document   : guestbookretrievebyname
    Created on : 26.11.2019, 11:46:01
    Author     : dieter
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.List"%>
<%-- Import-Code einfügen --%>
<%@page import="jeelernen.guestbook.ejb.GuestbookEngineAclEJBeanIfRemote"%>
<%@page import="javax.naming.InitialContext"%>
<%@page import="javax.naming.NamingException"%>
<%@page import="javax.ejb.EJBException"%>
<%@page import="jeelernen.guestbook.common.GuestbookEntry"%>

<%! String title = "GuestbookWarAclPrj- Client als Web-Anwendung - Ergebnis Suche nach Autor";
    String jndiguestbookengineRemote = "jeelernen.guestbook.ejb.GuestbookEngineAclEJBeanIfRemote";
    // Code einfügen

%>
<%

    String debugmsg = "";
    String resultlist = "";
    debugmsg = "Debug (" + new Date() + "): <br />";

    GuestbookEngineAclEJBeanIfRemote engine = null; // Code ändern/einfügen 
    // ggf. Code einfügen

    InitialContext ctx = null;
    try {
        ctx = new InitialContext();
        engine = (GuestbookEngineAclEJBeanIfRemote) ctx.lookup(jndiguestbookengineRemote); // Code ändern/einfügen 
        // Code einfügen  
    } catch (NamingException ne) {
        throw new EJBException(ne);

    } catch (Exception e) {
        throw new EJBException(e);
    }

    String authorStr = request.getParameter("name");
// if (authorStr == null) {
    // authorStr = "";
    //}

    if (engine != null && authorStr != null && !authorStr.equals("")) {

        List<GuestbookEntry> list = engine.getList(); // Code ändern/einfügen 
        if (list == null || list.size() == 0) {
            resultlist = "Keine Eintr&auml;ge gefunden.<br />";
        } else {
            resultlist = "<table>";
            resultlist = resultlist + "<tr>";
            resultlist = resultlist + "<td>Nr</td>";
            resultlist = resultlist + "<td>Id</td>";
            resultlist = resultlist + "<td>Autor</td>";
            resultlist = resultlist + "<td>Text</td>";
            resultlist = resultlist + "<td>Datum</td>";
            resultlist = resultlist + "</tr>";

            for (int i = 0; i < list.size(); i++) {
                // Code einfügen

                GuestbookEntry e = list.get(i);    // Code ändern/einfügen 
                if (e.getAuthor().equalsIgnoreCase(authorStr.trim())) {
                    resultlist = resultlist + "<tr>";
                    resultlist = resultlist + "<td>" + i + "</td>";
                    resultlist = resultlist + "<td>" + "Id: " + e.getId() + "</td>"; // Code ändern/einfügen 
                    resultlist = resultlist + "<td>" + "Author: " + e.getAuthor() + "</td>"; // Code ändern/einfügen 
                    resultlist = resultlist + "<td>" + "Msg: " + e.getMsg() + "</td>"; // Code ändern/einfügen 
                    resultlist = resultlist + "<td>" + "Date: " + e.getDate() + "</td>"; // Code ändern/einfügen 
                    resultlist = resultlist + "</tr>";

                }
            }
            resultlist = resultlist + "</table>";
        }
    } else {
        debugmsg = debugmsg + "Keine Referenz auf engineEJB vorhanden.<br/>";
        engine = null;
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%=title%></title>
        <style type="text/css">
            .code {color:blue; font-family: monospace}
            table {border-style: solid; border-width: 1px;}
        </style>
    </head>
    <body>
        <h3><%=title%></h3>
        <p>Suchbegriff: &quot;<span><%=authorStr%>&quot;</p>
        <div id="resultlist"><%=resultlist%></div>
        <hr />
        <p><a href="guestbookoperations.jsp">Zum den Operationen</p>
        <hr />
        <p id="debugmsg"><%=debugmsg%></p>
    </body>
</html>

