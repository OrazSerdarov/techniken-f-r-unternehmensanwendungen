<%-- 
    Document   : logout-frame
    Created on : 09.12.2019, 22:21:28
    Author     : dieter
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%!
String title = "GuestbookWarAclPrj-Client als Web-Anwendung";
%>
<% String logmsg="";
  // Code einfügen 
  
    logmsg = request.getUserPrincipal().getName() + " hat sich abgemeldet.";
    request.logout();
  %>

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><%=title%></title>
  </head>
  <body>
    <h1>Logout</h1>
    <p><%=logmsg%></p>
    
    <p>   <a href="guestbookoperations.jsp">Zu den Operationen</a></p>
  </body>
</html>
