/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jeelernen.guestbook.ejb;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.ExcludeClassInterceptors;
import javax.interceptor.Interceptors;
import jeelernen.guestbook.GuestbookEntry;
import jeelernen.utils.MyLogger;
import javax.annotation.security.RolesAllowed;


/**
 *
 * @author vmuser
 */

@Stateless
@Interceptors({MyLogger.class})
public class GuestbookAdminEJBean implements GuestbookAdminAclEJBeanIfRemote{

      @EJB
    private GuestbookEngineEJBeanIfLocal adminEJBean;
    private int countDelete ;
    private int countModyfy;
    private int countCreate;

    @ExcludeClassInterceptors
    public GuestbookAdminEJBean() {
        countDelete=0;
        countModyfy=0;
        countCreate=0;

    }


    @RolesAllowed({"admin","user"})
    @Override
    public GuestbookEntry createEntry(String author, String msg) {
        countCreate++;
        return adminEJBean.createEntry(author, msg);
    }


    @RolesAllowed({"admin", "user"})
    @Override
    public GuestbookEntry modifyEntry(int id, String author, String msg) {
        countModyfy++;
        return adminEJBean.modifyEntry(id, author, msg);
    }

    @RolesAllowed({"admin"})
    @Override
    public GuestbookEntry deleteEntry(int id) {
        countDelete++;
        return adminEJBean.deleteEntry(id);
    }

    @Override
    public int getCountCreateEntry() {
        return this.countCreate;
    }

    @Override
    public int getCountModifyEntry() {
       return this.countModyfy;
    }

    @Override
    public int getCountDeleteEntry() {
        return this.countDelete;
    }

    @Override
    public int getCountTotal() {
     return this.countCreate + this.countDelete + this.countModyfy;
    }

}