/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jeelernen.utils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

/**
 *
 * @author dieter
 */
public class MyLogger {

    @Resource
    EJBContext ejbc;

    @AroundInvoke
    public Object writeLog(InvocationContext ivcx) {

        try {
            String line = new Date().toString() + "  ";
            line = line + "  Called   " + ivcx.getMethod().getDeclaringClass().getSimpleName() + "::" + ivcx.getMethod().getName() + "\n";
            line = line + " By user: " + ejbc.getCallerPrincipal().getName();
            line = line + getRoles();

            MyLogger.write(line);
            return ivcx.proceed();
        } catch (Exception e) {
            return "Exception:   " + e.getMessage();
        }

    }

    static public void write(String linewithoutnewline) {

        String userhomepath = System.getProperty("user.home");
        String sep = System.getProperty("file.separator");
        String netbeansdir = "NetBeansProjects";
        String logdir = "log";
        String logfile = "guestbookearaclprj.log";
        String path = userhomepath + sep + netbeansdir + sep + logdir + sep + logfile;

        try (BufferedWriter r = new BufferedWriter(new FileWriter(path, true))) {
            r.write(linewithoutnewline + System.lineSeparator());
            r.close();
        } catch (IOException e) {
            System.out.println("Error: e=" + e.getClass().getName() + " msg=" + e.getMessage());
        }

    }

    private String getRoles() {
        String ret = "   ";
        ret = ret + "roles(admin,user):   ";
        if (isAdmin()) {
            ret = ret + "true,";
        } else {
            ret = ret + "false,";
        }

        if (isUser()) {
            ret = ret + "true";
        } else {
            ret = ret + "false";
        }
        return ret;
    }

    private boolean isAdmin() {

        return ejbc.isCallerInRole("admin");
    }

    private boolean isUser() {

        return ejbc.isCallerInRole("user");
    }
}
