/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jeelernen.utils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author dieter
 */
public class MyLogger {

  static public void write(String linewithoutnewline) {
   
    String userhomepath = System.getProperty("user.home");
    String  sep=System.getProperty("file.separator");
    String netbeansdir = "NetBeansProjects";
    String logdir = "log";
    String logfile = "guestbookearaclprj.log";
    String path = userhomepath + sep+ netbeansdir + sep+ logdir + sep+ logfile;
    
    try (BufferedWriter r = new BufferedWriter(new FileWriter(path, true))) {
      r.write(linewithoutnewline+System.lineSeparator());
      r.close();
    } catch (IOException e) {
      System.out.println("Error: e="+e.getClass().getName() + " msg="+e.getMessage());
    }

  }

}
