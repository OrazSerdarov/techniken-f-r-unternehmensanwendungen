/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jeelernen.guestbook.ejb.common;

/**
 *
 * @author dieter
 */

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import jeelernen.guestbook.common.GuestbookEntry;

/**
 *
 * @author dieter
 */
public class GuestbookEntries {

 public static final String DATA_STORE = "guestbookdatastoreejb.txt";
 private List<GuestbookEntry> list = new ArrayList<GuestbookEntry>();
 private int idcounter = 0;

 public int getIdcounter() {
  return idcounter;
 }

 public void setIdcounter(int val) {
  idcounter = val;
 }

 public void addEntry(GuestbookEntry entry) {
  list.add(entry);
 }

 public List<GuestbookEntry> getEntryList() {
  return list;
 }

 public void setEntryList(List<GuestbookEntry> val) {
  list = val;
 }

 public void clearEntryList() {
  list.clear();
 }

 public void deleteEntry(GuestbookEntry obj) {
  list.remove(obj);
 }

 public String loadFromFile() {
  String res = null;
  try {
   FileInputStream os = new FileInputStream(DATA_STORE);
   XMLDecoder decoder = new XMLDecoder(os);
   try {
    GuestbookEntries tmp = (GuestbookEntries) decoder.readObject();
    decoder.close();
    if (tmp != null) {
     list = tmp.getEntryList();
     idcounter = tmp.getIdcounter();
    } else {
     res = "Warnung: Leere Liste: " + DATA_STORE;
    }
   } catch (Exception e) {
    res = "Error: " + e.getMessage() + " + DATA_STORE";
   }
   os.close();

  } catch (IOException e) {
   res = "Cannot find or open File for reading: " + DATA_STORE;
  }
  return res;
 }

 public String saveToFile() {
  String res = null;
  try {
   FileOutputStream os = new FileOutputStream(DATA_STORE);
   XMLEncoder encoder = new XMLEncoder(os);
   encoder.writeObject(this);
   encoder.close();
   os.close();
  } catch (IOException e) {
   res = "Cannot find or open File for writing: " + DATA_STORE;
  }
  return res;
 }

 public GuestbookEntry getEntryById(int id) {
  GuestbookEntry res = null;
  // search for entry with id
  int size = list.size();
  for (int i = 0; i < size; i++) {
   res = list.get(i);
   if (res.getId() == id) {
    // found
    break;
   }
   res = null;  // entry not found
  } // for
  return res;
 }
}

