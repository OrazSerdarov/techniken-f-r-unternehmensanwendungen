/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jeelernen.guestbook.common;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author dieter
 */
public class GuestbookEntry implements Serializable{
 private static final long serialVersionUID = 20121024L;
    private int id;
    private String author;
    private String msg;
   private Date date;
   private boolean readonly=true;
    
    public int getId(){return id;}
    public void setId(int val){id=val;}
    public String getAuthor(){return author;}
    public void setAuthor(String val){author=val;}
    public String getMsg(){return msg;}
    public void setMsg(String val){msg=val;}
    public Date getDate(){return date;}
    public void setDate(Date val){date=val;}
    public boolean getReadonly(){return readonly;}
    public void setReadonly(boolean val){readonly=val;}

}

