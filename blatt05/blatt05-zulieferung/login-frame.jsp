<%-- 
    Document   : login-frame
    Created on : 09.12.2019, 22:18:51
    Author     : dieter
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<%!
String title = "GuestbookWarAclPrj-Client als Web-Anwendung";
%>
<% String logmsg="";
  // Code einfügen 
  
  %>

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><%=title%></title>
  </head>
  <body>
    <h1>Login</h1>
    <form method="post" action="#">
      <table>
        <tr>
          <td>Benutzername:</td><td><input type="text" name="username"/></td>
        </tr>
        <tr>
          <td>Passwort:</td><td><input type="text" name="password"/></td>
        </tr>
        <tr>
          <td><input type="submit" value="Absenden"></td><td>&nbsp;</td>
        </tr>
      </table>      
    </form>
    <p><%=logmsg%></p>
    <p>   <a href="guestbookoperations.jsp">Zu den Operationen</a></p>
  </body>
</html>