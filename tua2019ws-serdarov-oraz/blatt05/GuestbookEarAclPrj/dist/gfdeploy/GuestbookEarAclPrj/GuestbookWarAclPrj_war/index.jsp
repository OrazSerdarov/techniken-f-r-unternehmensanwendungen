<%-- 
    Document   : index
    Created on : 06.11.2013, 11:37:30
    Author     : dieter
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%!
String title="GuestbookWarAclPrj- Client als Web-Anwendung - Startseite";
%>
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title><%=title%></title>
 </head>
 <body>
  <h3><%=title%></h3>
  <p>
  <a href="guestbookoperations.jsp">Zu den Operationen</a>
  </p>
  <p><%=application.getRealPath("/")%></p>
 </body>
</html>


