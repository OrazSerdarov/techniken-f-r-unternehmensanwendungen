<%-- 
    Document   : guestbookoperations
    Created on : 26.11.2019, 11:46:01
    Author     : dieter
--%>

<%@page import="javax.naming.InitialContext"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.List"%>
<%-- Import-Code einfügen --%>
<%@page import="javax.ejb.EJBException"%>
<%@page import="javax.naming.NamingException"%>
<%@page import="jeelernen.guestbook.ejb.GuestbookEngineAclEJBeanIfRemote"%>
<%@page import="jeelernen.guestbook.ejb.GuestbookAdminAclEJBeanIfRemote"%>
<%@page import="jeelernen.guestbook.GuestbookEntry"%>
<%@page import="javax.ejb.EJBAccessException"%>


<!DOCTYPE html>
<%! String title = "GuestbookWarAclPrj- Client als Web-Anwendung";
    String jndiguestbookengineRemote = "jeelernen.guestbook.ejb.GuestbookEngineAclEJBeanIfRemote";

// Code einfügen  
    String jndiguestbookadminRemote = "jeelernen.guestbook.ejb.GuestbookAdminAclEJBeanIfRemote";
    // Code einfügen

%>
<%
    Boolean cmdFound = false;
    String resultid = "&nbsp;";
    String resultname = "&nbsp;";
    String resulttext = "&nbsp;";
    String resultdate = "&nbsp;";
    int countCreate = 0;
    int countModify = 0;
    int countDelete = 0;
    String logmsg = "Meldung (" + new Date() + "): <br />";
    String debugmsg = "Debug (" + new Date() + "): <br />";
    String username = "";
    //mein Code
    GuestbookEngineAclEJBeanIfRemote engine = null;
    GuestbookAdminAclEJBeanIfRemote admin = null;
    GuestbookEntry resultObj = null;
    InitialContext ctx = null;

    try {
        ctx = new InitialContext();
        engine = (GuestbookEngineAclEJBeanIfRemote) ctx.lookup(jndiguestbookengineRemote); // Code ändern/einfügen 
        admin = (GuestbookAdminAclEJBeanIfRemote) ctx.lookup(jndiguestbookadminRemote);; // Code ändern/einfügen 

        // Code einfügen  
    } catch (NamingException ne) {
        throw new EJBException(ne);

    } catch (Exception e) {
        throw new Exception(e);
    }

    String cmdCreate = request.getParameter("create");
    String cmdModify = request.getParameter("modify");
    String cmdDelete = request.getParameter("delete");
    String cmdSearchById = request.getParameter("retrievebyid");

    if (admin != null) {

        try {

            if (cmdCreate != null) {
                cmdFound = true;
                String authorStr = request.getParameter("name");
                String msgStr = request.getParameter("msg");
                resultObj = admin.createEntry(authorStr, msgStr);
                // Code einfügen
            }
            if (cmdModify != null) {
                cmdFound = true;
                String idStr = request.getParameter("identry");
                String authorStr = request.getParameter("name");
                String msgStr = request.getParameter("msg");
                int idInt = -1;
                try {
                    idInt = Integer.parseInt(idStr);
                } catch (NumberFormatException e) {
                    logmsg += "ID soll eine Zahl sein!";
                }

                if (idInt >= 0) {
                    resultObj = admin.modifyEntry(idInt, authorStr, msgStr);
                } else {
                    logmsg += "Keine Gültige ID!";
                }
                // Code einfügen
            }
            if (cmdDelete != null) {
                cmdFound = true;
                String idStr = request.getParameter("identry");

                int idInt = -1;
                try {
                    idInt = Integer.parseInt(idStr);
                } catch (NumberFormatException e) {
                    logmsg += "ID soll eine Zahl sein!";
                }

                if (idInt >= 0) {
                    resultObj = admin.deleteEntry(Integer.parseInt(idStr));
                } else {
                    logmsg += "Keine Gültige ID!";
                }

                // Code einfügen
            }

        } catch (EJBAccessException ae) {
            logmsg += "Sie haben keine Rechte um diese Operation ausführen zu dürfen!<br/>";

        } catch (Exception e) {
            debugmsg += "Eine Exception in admin ist aufgetreten.";
        }

    } //  if (admin != null)

    if (engine != null) {
        if (cmdSearchById != null) {
            cmdFound = true;
            String idStr = request.getParameter("identry");

            int idInt = -1;
            try {
                idInt = Integer.parseInt(idStr);
            } catch (NumberFormatException e) {
                logmsg += "ID soll eine Zahl sein!";
            }

            if (idInt >= 0) {
                resultObj = engine.searchById(Integer.parseInt(idStr));
            } else {
                logmsg += "Keine Gültige ID!";
            }

            // Code einfügen
        }
        if (cmdFound == false) {
            logmsg += "Unbekanntes Kommando. Nichts getan.<br/>";
        }
    } // if (engine != null)

    if (resultObj != null) {
        resultid = String.valueOf(resultObj.getId());
        resulttext = resultObj.getMsg();
        resultdate = resultObj.getDate().toString();
        resultname = resultObj.getAuthor();
    }

    if (admin != null) {
        // update counts
        countCreate = admin.getCountCreateEntry(); // Code ändern/einfügen
        countModify = admin.getCountModifyEntry(); // Code ändern/einfügen
        countDelete = admin.getCountDeleteEntry(); // Code ändern/einfügen
    } else {
        debugmsg += "EJBean ist nicht erreichbar.<br/>";
    }
    String requestURI = request.getRequestURI();

    if (request.getUserPrincipal() != null) {
        username = request.getUserPrincipal().getName();
    }


%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%=title%></title>
        <style type="text/css">
            .code {color:blue; font-family: monospace}
            table {border-style: solid; width: 1px;}
        </style>
    </head>
    <body>
        <h3><%=title%></h3>

        <table style="width:600px"><tr>
                <td style="width:100px"><a href="login.jsp">Login</a></td>
                <td style="width:100px"><a href="logout.jsp">Logout</a></td>         
                <td > Eingeloggter Benutzer: <%=username%></td>
            </tr>
        </table>


        <table><tr><td>
                    <h4>Alle Eintr&auml;ge auflisten</h4>
                    <form method="get" action="guestbooklistall.jsp">
                        <table><tr><td> <input type="submit" value="Auflisten" /> </td></tr></table>
                    </form>
                </td><td> 
                    <h4>Suche Eintrag per Namen</h4>
                    <form method="get" action="guestbookretrievebyname.jsp" >
                        <table>
                            <tr><td>Name</td><td><input type="text" name="name" value=""/></td>
                            </tr>
                            <tr><td>&nbsp;</td><td>
                                    <input type="submit" name="retrievebyname"value="Suchen" />
                                </td></tr></table>
                    </form>
                </td></tr>
            <tr><td>
                    <h4>Erzeuge Eintrag (<%=countCreate%>)</h4>
                    <form method="post" action="<%=requestURI%>" >
                        <table>
                            <tr><td>Name</td><td><input type="text" name="name" /></td>
                            </tr>
                            <tr><td>Text</td><td><input type="text" name="msg" /></td>
                            </tr>
                            <tr><td>&nbsp;</td><td><input type="submit" name="create" value="Erzeugen"/></td>
                            </tr>
                        </table>
                    </form>
                </td><td>
                    <h4>Modifiziere Eintrag (<%=countModify%>)</h4>
                    <form method="post" action="<%=requestURI%>" >
                        <table>
                            <tr>
                                <td>ID</td><td><input type="text" name="identry" /></td>
                            </tr>
                            <tr>
                                <td>Name</td><td><input type="text" name="name" /></td>
                            </tr>
                            <tr><td>Text</td><td><input type="text" name="msg" /></td>
                            </tr>
                            <tr><td>&nbsp;</td><td><input type="submit" name="modify" value="Modifizieren"/></td>
                            </tr>
                        </table>
                    </form>
                </td></tr>
            <tr><td>
                    <h4>L&ouml;sche Eintrag (<%=countDelete%>)</h4>
                    <form method="post" action="<%=requestURI%>" >
                        <table>
                            <tr><td>ID</td><td><input type="text" name="identry" value=""/></td>
                            </tr>
                            <tr><td>&nbsp;</td><td><input type="submit" name="delete" value="L&ouml;schen"/></td>
                            </tr>
                        </table>
                    </form>
                </td><td>
                    <h4>Hole Eintrag per ID</h4>
                    <form method="post" action="<%=requestURI%>" >
                        <table>
                            <tr><td>ID</td><td><input type="text" name="identry" value=""/></td>
                            </tr>
                            <tr><td>&nbsp;</td><td><input type="submit" name="retrievebyid" value="Holen"/></td>
                            </tr>
                        </table>
                    </form>
                </td></tr>
        </table>
        <p>Resultat der letzten Aktion:</p>
        <table style="width:600px">
            <tr><td style="width:100px;">ID:</td><td id="resultid"><%=resultid%></td></tr>
            <tr><td>Name:</td><td id="resultname"><%=resultname%></td></tr>
            <tr><td>Text:</td><td id="resulttext"><%=resulttext%></td></tr>
            <tr><td>Date:</td><td id="resultdate"><%=resultdate%></td></tr>
        </table>
        <p id="msgp"><%=logmsg%></span></p>
    <hr />
    <p id="msgp"><%=debugmsg%></span></p>
</body>
</html>

