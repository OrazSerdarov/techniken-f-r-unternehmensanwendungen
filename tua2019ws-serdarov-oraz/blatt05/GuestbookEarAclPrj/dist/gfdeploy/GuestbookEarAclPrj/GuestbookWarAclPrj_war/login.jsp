<%-- 
    Document   : login-frame
    Created on : 09.12.2019, 22:18:51
    Author     : dieter
--%>

<%@page import="com.sun.xml.ws.security.policy.UserNameToken"%>
<%@page import="javax.annotation.Resource"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="jeelernen.guestbook.ejb.GuestbookAdminAclEJBeanIfRemote"%>
<%@page import="javax.naming.InitialContext"%>
<%@page import="javax.naming.NamingException"%>
<%@page import="javax.ejb.EJBException"%>
<!DOCTYPE html>
<%!
    String title = "GuestbookWarAclPrj-Client als Web-Anwendung";
%>
<% String logmsg = "";
    // Code einfügen 

    String userName = request.getParameter("username");
    String userPassword = request.getParameter("password");

    if (userName!=null && userPassword!=null) {

        if (request.getUserPrincipal() == null) {
            try {
                request.login(userName, userPassword);
                logmsg = "Sie haben sich als " + request.getUserPrincipal() + " angemeldet.";
             
            } catch (ServletException se) {
                logmsg = "Login hat fehlgeschlagen. \n" + se.getMessage();
            }

        } else {
            logmsg = "Sie sind bereits als  " + request.getUserPrincipal() + " eingeloggt.";
        }
    }
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%=title%></title>
    </head>
    <body>
        <h1>Login</h1>
        <form method="post" action="#">
            <table>
                <tr>
                    <td>Benutzername:</td><td><input type="text" name="username"/></td>
                </tr>
                <tr>
                    <td>Passwort:</td><td><input type="text" name="password"/></td>
                </tr>
                <tr>
                    <td><input type="submit" value="Absenden"></td><td>&nbsp;</td>
                </tr>
            </table>      
        </form>
        <p><%=logmsg%></p>
        <p>   <a href="guestbookoperations.jsp">Zu den Operationen</a></p>
    </body>
</html>

