

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jeelernen.guestbook.ejb;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import javax.ejb.Stateless;
import jeelernen.guestbook.GuestbookEntry;



/**
 *
 * @author vmuser
 */
@Stateless
public class GuestbookEngineEJBean implements GuestbookEngineAclEJBeanIfRemote, GuestbookEngineEJBeanIfLocal {
    
    List<GuestbookEntry> guestbookEntries;
     private static AtomicInteger unigueId = new AtomicInteger();
    public GuestbookEngineEJBean() {
        guestbookEntries = new ArrayList<GuestbookEntry>();
    }
    
    @Override
    public GuestbookEntry searchById(int id) {
        GuestbookEntry anEntry = null;
        for (GuestbookEntry ge : guestbookEntries) {
            if (ge.getId() == id) {
                anEntry = ge;
                break;
            }
        }
        return anEntry;
        
    }
    
    @Override
    public List<GuestbookEntry> searchByAuthor(String author) {
        List<GuestbookEntry> aList = new ArrayList<GuestbookEntry>();
        
        for(GuestbookEntry ge : guestbookEntries) {
            if (ge.getAuthor().equalsIgnoreCase(author)) {
                aList.add(ge);
            }
        }
        
        return aList;
    }
    
    @Override
    public List<GuestbookEntry> getList() {
        
        return guestbookEntries;
    }
    
    @Override
    public GuestbookEntry createEntry(String author, String msg) {
        
        GuestbookEntry newEntry = new GuestbookEntry(author, msg , unigueId.getAndIncrement(), new Date());
        guestbookEntries.add(newEntry);
        return newEntry;
    }
    
    @Override
    public GuestbookEntry modifyEntry(int id, String author, String msg) {
        GuestbookEntry anEntry = searchById(id);
        
        if (anEntry != null) {
            anEntry.setAuthor(author);
            anEntry.setMsg(msg);
            guestbookEntries.set(id, anEntry);
            
        }
        return anEntry;
    }
    
    @Override
    public GuestbookEntry deleteEntry(int id) {
        GuestbookEntry anEntry = searchById(id);
        if (anEntry != null) {
            guestbookEntries.remove(anEntry);
        }
        return anEntry;
        
    }
}


