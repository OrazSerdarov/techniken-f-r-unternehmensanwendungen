#!/bin/bash
set -u
XML1=a01-roomschedule.xml 
XSLT1=a01-empty.xsl
XML2=a02-roomschedule.xml 
XSLT2=a02-roomschedule.xsl 
XML3=a03-roomlist.xml
XSLT3=a03-roomlist.xsl
XML4=a04-roomlist.xml
XSLT4=a04-roomlist.xsl
XML5=a05-empty.xml
XSLT5=a05-merge-files-fixed-root.xsl
XSLPATH=xslt
OPTCONFIG="--config"
OPTOUTPUT="--output"
OUTPUTFILE=""

usage() {
echo "Usage: $0 $OPTCONFIG=<nb> [--output=<FILE>]"
	echo " <nb>=1 : transforming $XML1 with $XSLT1" 
	echo " <nb>=2 : transforming $XML2 with $XSLT2" 
	echo " <nb>=3 : transforming $XML3 with $XSLT3" 
	echo " <nb>=4 : transforming $XML4 with $XSLT4" 
	echo " <nb>=5 : transforming $XML5 with $XSLT5" 
}
if [ "$#" = 0 ]; then usage; exit 1; fi

# get option name as the prefix in front of character =
PRFX="${1%%=*}"
#echo PRFX=$PRFX

if [ "$PRFX" != "$OPTCONFIG" ]; then usage; exit 1; fi

# get value of option
SUFFX="${1#*=}"
#echo SUFFX=$SUFFX

case "$SUFFX" in
	1) XMLFILE=$XML1 XSLTRANSFORM=$XSLT1;;
	2) XMLFILE=$XML2 XSLTRANSFORM=$XSLT2;;
	3) XMLFILE=$XML3 XSLTRANSFORM=$XSLT3;;
	4) XMLFILE=$XML4 XSLTRANSFORM=$XSLT4;;
	5) XMLFILE=$XML5 XSLTRANSFORM=$XSLT5;;
	*) 	echo "ERROR: Found unknown value for $OPTCONFIG='$SUFFX'"; usage; exit 1;;
esac

if [ $# -gt 1 ]; then 
# get option output as the prefix in front of character =
PRFX="${2%%=*}"
  if [ "$PRFX" != "$OPTOUTPUT" ]; then 
    echo "WARNING: Unknown option $2 is ignored"
  else
    # get value of option
    SUFFX="${2#*=}"
    OUTPUTFILE="$SUFFX"
  fi
fi

XSLTOUTPUT="";
if [ "$OUTPUTFILE" != "" ]; then 
   XSLTOUTPUT="--output results/$OUTPUTFILE"
fi

CMD="xsltproc $XSLTOUTPUT $XSLPATH/$XSLTRANSFORM $XMLFILE"
echo "--- Going to execute: $CMD"
$CMD
