<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:r="http://www.hs-coburg.de/xmllernen/roomlist"
    >
     <xsl:strip-space elements="*" />
         <!-- strip-space: Strips text nodes with whitespace content from source tree. -->
         <!-- preserve-space: Preserves text nodes with whitespace content in source tree. -->
         <!-- In the following example: The matches for r:misc have the same precedence. 
             The order of appearance is relevant. 
             The first match will be the applied one -->
         <!--            <xsl:preserve-space elements="r:misc" /> -->
         <!-- <xsl:strip-space elements="r:room r:location r:misc" /> -->
</xsl:stylesheet>
