﻿<?xml version="1.0"  encoding="UTF-8" ?>
<xsl:stylesheet  version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:r="http://www.hs-coburg.de/xmllernen/roomschedule" >   
<xsl:output method="text" version="1.0" />


<xsl:template match="/">
        <xsl:text disable-output-escaping="no">Benutzte R&#228;ume:  &#13;</xsl:text>
        <xsl:apply-templates select="r:roomschedule/r:entry" />
</xsl:template>


<xsl:template match="r:entry">
  <xsl:text disable-output-escaping="yes">###  Raum </xsl:text>
        <xsl:apply-templates select="r:roomid" />
        <xsl:apply-templates select="r:weekdate |r:singledate" />
  <xsl:text disable-output-escaping="no">&#13;</xsl:text>
</xsl:template>

<xsl:template match="r:roomid">
          <xsl:text disable-output-escaping="yes"># Nummer:</xsl:text>
        <xsl:value-of select="text()" />
</xsl:template>

<xsl:template match="r:weekdate | r:singledate">
        <xsl:text disable-output-escaping="yes">#  Zeit: </xsl:text>
        <xsl:value-of select="@day" />
        <xsl:text disable-output-escaping="yes">  </xsl:text>
        <xsl:value-of select="@startTime" />
        <xsl:text disable-output-escaping="yes">-</xsl:text>
        <xsl:value-of select="@endTime" />
</xsl:template>






</xsl:stylesheet>