﻿<?xml version="1.0"  encoding="UTF-8" ?>
<xsl:stylesheet  version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:r="http://www.hs-coburg.de/xmllernen/roomlist" >   
<xsl:output method="html" doctype-system="about:legacy-compat" />


<xsl:template match="/">
<html>
    <body>
        <h1>Vorhandene R&#228;ume:</h1>
          <table style=" border: 3px solid red; " >
              <tr >
                    <th style=" border: 3px solid red;">Nummer</th>
                    <th style=" border: 3px solid red; " >Geb&#228;ude</th>
                    <th style=" border: 3px solid red; " >Stockwerk</th>
                    <th style=" border: 3px solid red;  text-align:left;"  >Typ</th>
                    <th style=" border: 3px solid red; " >Pltze</th>
                      
              </tr>
                <xsl:apply-templates select="r:roomlist/r:room" />
        </table>
    </body>
</html>
</xsl:template>





<xsl:template match="r:room">
  <xsl:element name="tr">
                <xsl:element name="td">
                        <xsl:attribute name="style">border: 3px solid blue; </xsl:attribute>
                        <xsl:value-of select="@number" />
                </xsl:element>
                                <xsl:apply-templates select="r:location/r:building | r:location/r:level " />
                <xsl:element name="td">
                        <xsl:attribute name="style">border: 3px solid blue; </xsl:attribute>
                        <xsl:value-of select="@type" />
                </xsl:element>
                                <xsl:apply-templates select="r:seats " />
  </xsl:element>
</xsl:template>

<xsl:template match="r:building | r:level  | r:seats ">
                <xsl:element name="td">
                         <xsl:attribute name="style">border: 3px solid blue; </xsl:attribute>
                        <xsl:value-of select="." />
                </xsl:element>
</xsl:template>




</xsl:stylesheet>
