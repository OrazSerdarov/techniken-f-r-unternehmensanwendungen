﻿<?xml version="1.0"  encoding="UTF-8" ?>
<xsl:transform  version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:r="http://www.hs-coburg.de/xmllernen/roomlist" >   
<xsl:output method="xml" version="1.0" encoding="UTF-8" />



<xsl:template match="/">
    <xsl:element name="r:roomlist">
   
                      <xsl:apply-templates selectr="r:room" ></xsl:apply-templates>
             
    </xsl:element>
</xsl:template>



<xsl:template match="r:room">
   
   <xsl:copy >
        <xsl:copy-of select="@type" />
            <xsl:text>&#10; </xsl:text> 
        <xsl:for-each select="comment()">
                <xsl:copy-of select="." />
                <xsl:text>&#10; </xsl:text> 
        </xsl:for-each>
        <xsl:text>&#10; &#32;</xsl:text> 

                
      <xsl:element name="r:id">
              <xsl:value-of select="@number" />      
      </xsl:element>      
      <xsl:text>&#10;  </xsl:text> 
      
      <xsl:apply-templates select="r:location"  />
      <xsl:text>&#10; </xsl:text> 
    
      <xsl:element name="r:properties">
              <xsl:text>&#10; </xsl:text> 
              <xsl:apply-templates select="r:propertyAccess | r:propertyHeight" />
      </xsl:element>
       <xsl:text>&#10; </xsl:text> 
       
      <xsl:element name="r:misc" >
      </xsl:element>
        
        <xsl:text>&#10; </xsl:text> 
    
  </xsl:copy>

</xsl:template>


<xsl:template match="r:location ">
      <xsl:copy>
            <xsl:for-each select="node()">
                  <xsl:if test="local-name()='building' ">
               <xsl:attribute name="building">  <xsl:value-of select="text()" />   </xsl:attribute> 
               </xsl:if>
               <xsl:if test="local-name()='level' ">
               <xsl:attribute name="level">  <xsl:value-of select="text()" />  </xsl:attribute> 
               </xsl:if>
            </xsl:for-each>
            
      </xsl:copy>
</xsl:template>

<xsl:template match="r:propertyAccess | r:propertyHeight">
      <xsl:copy>
            <xsl:value-of select="text()" />
      </xsl:copy>    
     <xsl:text>&#10; </xsl:text>
</xsl:template>






</xsl:transform>
